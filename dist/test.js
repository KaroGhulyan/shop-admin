// // Application div
    // const appDiv = "content";
    // // Both set of different routes and template generation functions
    // let routes = {};
    // let templates = {};
    // // Register a template (this is to mimic a template engine)
    // let template = (name, templateFunction) => {
    //     return templates[name] = templateFunction;
    // };
    // // Define the routes. Each route is described with a route path & a template to render
    // // when entering that path. A template can be a string (file name), or a function that
    // // will directly create the DOM objects.
    // let route = (path, template) => {
    //     if (typeof template == "function") {
    //         return routes[path] = template;
    //     }
    //     else if (typeof template == "string") {
    //         return routes[path] = templates[template];
    //     }
    //     else {
    //         return;
    //     }
    // };

    // // Register the templates.
    // template('template1', () => {
    //     let myDiv = document.getElementById(appDiv);
    //     myDiv.innerHTML = "";

    // });

    // //Products
    // let products = shopDataBase.products.getAll();
    // let productsKey = Object.keys(products[0]);
    // template('template-product', () => {
    //     let myDiv = document.getElementById(appDiv);
    //     myDiv.innerHTML = "";
    //     const link1 = createDiv('product', render.render(shopDataBase.products.name, products, productsKey));
    //     return myDiv.appendChild(link1);
    // });
    // //Categories
    // let categories = shopDataBase.categories.getAll();
    // let categoriesKey = ['id', 'name', 'remove'];
    // template('template-categories', () => {
    //     let myDiv = document.getElementById(appDiv);
    //     myDiv.innerHTML = "";
    //     const link2 = createDiv('categories', render.render(shopDataBase.categories.name, categories, categoriesKey));
    //     return myDiv.appendChild(link2);
    // });
    // //Attributes-Groups
    // let attributesGroups = shopDataBase.attributeGroups.getAll();
    // let attributesGroupsKey = ['id', 'name', 'remove'];
    // template('template-attributes-groups', () => {
    //     let myDiv = document.getElementById(appDiv);
    //     myDiv.innerHTML = "";
    //     const link2 = createDiv('attributes-groups', render.render(shopDataBase.attributeGroups.name, attributesGroups, attributesGroupsKey));
    //     return myDiv.appendChild(link2);
    // });
    // //Attributes
    // let attributes = shopDataBase.attributes.getAll();
    // let attributesKey = ['id', 'name', 'remove'];
    // template('template-attributes', () => {
    //     let myDiv = document.getElementById(appDiv);
    //     myDiv.innerHTML = "";
    //     const link2 = createDiv('attributes', render.render(shopDataBase.attributes.name, attributes, attributesKey));
    //     return myDiv.appendChild(link2);
    // });
    // let attributesValues = shopDataBase.attributesValues.getAll();
    // let attributesValuesKeys = ['id', 'name', 'remove'];
    // template('template-attributes-values', () => {
    //     let myDiv = document.getElementById(appDiv);
    //     myDiv.innerHTML = "";
    //     const link2 = createDiv('attributes-values', render.render(shopDataBase.attributesValues.name, attributesValues, attributesValuesKeys));
    //     return myDiv.appendChild(link2);
    // });
    // // Define the mappings route->template.
    // route('/', 'template1');
    // route('/product', 'template-product');
    // route('/categories', 'template-categories');
    // route('/attributes-groups', 'template-attributes-groups');
    // route('/attributes', 'template-attributes');
    // route('/attributes-values', 'template-attributes-values');

    // // Generate DOM tree from a string
    // let createDiv = (id, xmlString) => {
    //     let d = document.createElement('div');
    //     d.id = id;
    //     d.innerHTML = xmlString;
    //     return d.firstChild;
    // };
    // // Helper function to create a link.
    // let createLink = (title, text, href) => {
    //     let a = document.createElement('a');
    //     let linkText = document.createTextNode(text);
    //     a.appendChild(linkText);
    //     a.title = title;
    //     a.href = href;
    //     return a;
    // };

    // // Give the correspondent route (template) or fail
    // let resolveRoute = (route) => {
    //     try {
    //         return routes[route];
    //     } catch (error) {
    //         throw new Error("The route is not defined");
    //     }
    // };
    // // The actual router, get the current URL and generate the corresponding template
    // let router = (evt) => {
    //     const url = window.location.hash.slice(1) || "/";
    //     const routeResolved = resolveRoute(url);
    //     routeResolved();
    // };
    // // For first load or when routes are changed in browser url box.
    // window.addEventListener('load', router);
    // window.addEventListener('hashchange', router);