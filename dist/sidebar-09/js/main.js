(function ($) {

    "use strict";

    var fullHeight = function () {

        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function () {
            $('.js-fullheight').css('height', $(window).height());
        });

    };
    fullHeight();

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

})(jQuery);


// const pages = document.querySelectorAll(".custom-height");
// document.querySelectorAll(".components li a").forEach((element) =>
//     element.addEventListener("click", function (event) {
//         event.preventDefault();
//         console.log(pages[event.target.name - 1]);
//         document
//             .querySelector(".custom-height.active-page")
//             .classList.remove("active-page");
//         pages[event.target.name - 1].classList.add("active-page");
//     })
// );


// Application div
const appDiv = "content";
// Both set of different routes and template generation functions
let routes = {};
let templates = {};
// Register a template (this is to mimic a template engine)
let template = (name, templateFunction) => {
    return templates[name] = templateFunction;
};
// Define the routes. Each route is described with a route path & a template to render
// when entering that path. A template can be a string (file name), or a function that
// will directly create the DOM objects.
let route = (path, template) => {
    if (typeof template == "function") {
        return routes[path] = template;
    }
    else if (typeof template == "string") {
        return routes[path] = templates[template];
    }
    else {
        return;
    }
};

// Register the templates.
template('template1', () => {
    let myDiv = document.getElementById(appDiv);
    myDiv.innerHTML = "";

});
template('template-product', () => {
    let myDiv = document.getElementById(appDiv);
    myDiv.innerHTML = "";
    const link1 = createDiv('product', "<h1>This is Product </h1><a href='#/'>Go Back to Index</a>");
    return myDiv.appendChild(link1);
});
template('template-categories', () => {
    let myDiv = document.getElementById(appDiv);
    myDiv.innerHTML = "";
    const link2 = createDiv('categories', "<h1>This is Categories </h1><a href='#/'>Go Back to Index</a>");
    return myDiv.appendChild(link2);
});
template('template-attributes-groups', () => {
    let myDiv = document.getElementById(appDiv);
    myDiv.innerHTML = "";
    const link2 = createDiv('attributes-groups', "<h1>This is AttributesGroups </h1><a href='#/'>Go Back to Index</a>");
    return myDiv.appendChild(link2);
});
template('template-attributes', () => {
    let myDiv = document.getElementById(appDiv);
    myDiv.innerHTML = "";
    const link2 = createDiv('attributes', "<h1>This is Attributes </h1><a href='#/'>Go Back to Index</a>");
    return myDiv.appendChild(link2);
});
template('template-attributes-values', () => {
    let myDiv = document.getElementById(appDiv);
    myDiv.innerHTML = "";
    const link2 = createDiv('attributes-values', "<h1>This is AttributesValues </h1><a href='#/'>Go Back to Index</a>");
    return myDiv.appendChild(link2);
});
// Define the mappings route->template.
route('/', 'template1');
route('/product', 'template-product');
route('/categories', 'template-categories');
route('/attributes-groups', 'template-attributes-groups');
route('/attributes', 'template-attributes');
route('/attributes-values', 'template-attributes-values');

// Generate DOM tree from a string
let createDiv = (id, xmlString) => {
    let d = document.createElement('div');
    d.id = id;
    d.innerHTML = xmlString;
    return d.firstChild;
};
// Helper function to create a link.
let createLink = (title, text, href) => {
    let a = document.createElement('a');
    let linkText = document.createTextNode(text);
    a.appendChild(linkText);
    a.title = title;
    a.href = href;
    return a;
};

// Give the correspondent route (template) or fail
let resolveRoute = (route) => {
    try {
        return routes[route];
    } catch (error) {
        throw new Error("The route is not defined");
    }
};
// The actual router, get the current URL and generate the corresponding template
let router = (evt) => {
    const url = window.location.hash.slice(1) || "/";
    const routeResolved = resolveRoute(url);
    routeResolved();
};
// For first load or when routes are changed in browser url box.
window.addEventListener('load', router);
window.addEventListener('hashchange', router);