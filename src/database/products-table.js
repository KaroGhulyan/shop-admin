import BaseTable from './base-table';
import { shopDataBase } from './database'



export default class ProductTable extends BaseTable {
    constructor() {
        super('products');
    }
    _createProductObject({ id, name, price, category_id, }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            name,
            price,
            category_id,
            getNames() {
                let arr = [];
                arr.push(shopDataBase.categories.getById(this.category_id).name)
                return arr;
            },
            remove() {
                that._remove(this.id);
            }
        };
    }
    getByCategoryId(id) {
        return this.getAll().filter(item => item.category_id == id);
    }
    create(productData) {
        const product = this._createProductObject(productData);
        this._add(product);
        return product.id
    }
}
