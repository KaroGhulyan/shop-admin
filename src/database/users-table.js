import BaseTable from './base-table';
import { DB, shopDataBase } from './database'

export default class UsersTable extends BaseTable {
    constructor() {
        super('users');
    }
    _createUserObject({ id, name, surname, sex, age, email, adress }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            name,
            surname,
            sex,
            age,
            email,
            adress,
            remove() {
                that._remove(this.id);
            }
        };
    }
    create(Userinfo) {
        let user = this._createUserObject(Userinfo);
        this._add(user);
        // shopDataBase.save();
        // shopDataBase.load();

    }
}