// import DB from '../index';
import ProductTable from './products-table'
import { DB, shopDataBase } from './database'
// const shopDataBase = new DB("Shop Data Base");
function _idGenerate() {
    let i = 1;
    return function () {
        return i++;
    };
}



export default class BaseTable {

    constructor(tableName) {
        this.name = tableName;
        this._db = shopDataBase;
        this._data = [];
        this._idGenerate = _idGenerate();
    }
    _add(obj) {
        this._data.push({ ...obj });
        this._db.save();
    }
    _remove(id) {
        this._data.splice(this._data.indexOf(this.getById(id)), 1);
    }
    getAll() {
        return this._data;
    }
    getById(id) {
        return this._data.find(item => item.id == id);
    }
}