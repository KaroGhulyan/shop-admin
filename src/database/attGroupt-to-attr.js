import BaseTable from './base-table';
import { DB, shopDataBase } from './database'



export default class AttributeGroupsToAttribute extends BaseTable {
    constructor() {
        super('attributesGroupsToAttribute');
    }
    createObj({ id, attributeGroups_id, attribute_id }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            attributeGroups_id,
            attribute_id,
            remove() {
                // console.log(this.id)
                that._remove(this.id);
            }
        }
    }
    create({ attributeGroups_id, attribute_id }) {
        let attrtogroup = this.createObj({ attributeGroups_id, attribute_id });
        if (!shopDataBase.attributeGroups.getById(attributeGroups_id)) {
            throw `attributeGroup id is doesn't exists`;
        };
        if (attribute_id.length) {
            this._add(attrtogroup);
        }
        // this._add(attrtogroup);
    }

}