import BaseTable from './base-table';
import { DB, shopDataBase } from './database'


export default class AttributeGroupsTable extends BaseTable {
    constructor() {
        super('attributeGroups');
    }
    _createCategoryObject({ id, name }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            name,
            // products() {
            //     return that._db.products.getByCategoryId(this.id)
            // },
            getNames() {
                const names = this.attributes();
                let arr = names.map(element => {
                    if (element !== undefined) {
                        return element.name
                    }
                    // else {
                    //     let index = names.findIndex(element => element === undefined);

                    // }
                })
                return arr;
            },

            attributes() {
                let dataId = shopDataBase.attributesGroupsToAttribute.getAll();
                let arr = [];
                dataId.forEach(element => {
                    if (element.attributeGroups_id === this.id) {
                        element.attribute_id.forEach(id => {
                            arr.push(that._db.attributes._data.find(item => item.id === id))
                        })
                    }
                });
                return arr
            },
            addAtrToAttrGroup({ attribute_id, attributeGroups_id }) {
                that._db.attributesGroupsToAttribute.create({ attribute_id, attributeGroups_id });
            },
            remove() {
                // shopDataBase.attributesGroupsToAttribute.getAll().forEach((element, index, array) => {
                //   if (element.attributeGroups_id === this.id) {
                //     console.log(index)
                //     element.remove(this.id);
                //     index -= 1;

                //     console.log(element);
                //   }
                // });
                let arr = shopDataBase.attributesGroupsToAttribute.getAll();
                for (let i = 0; i < arr.length; i++) {
                    if (arr[i].attributeGroups_id === this.id) {
                        arr[i].remove(this.id);
                        i--;
                    }
                }
                that._remove(this.id);
            }
        }
    }

    create(AttributesGroupData) {
        let attrGroup = this._createCategoryObject(AttributesGroupData)
        this._add(attrGroup);
    }
}