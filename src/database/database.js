
class DB {
    constructor(dbName = 'my_db') {
        this.name = dbName;
        this.load();
    }
    addTables(tables) {
        for (let table of tables) {
            this[table.name] = table;
            // console.log(table);
        }
    }
    save() {
        let cache = [];
        localStorage.setItem(this.name, JSON.stringify(this, function (key, value) {
            if (typeof value === 'object' && value !== null) {
                if (cache.indexOf(value) !== -1) return;
                cache.push(value);
            };
            return value;
        }));
        cache = null;
    }
    load() {
        let data = localStorage.getItem(this.name);
        return data ? Object.assign(JSON.parse(data), this) : {};
    }
}
const shopDataBase = new DB("Shop Data Base");
// console.log(shopDataBase);

export { DB, shopDataBase };