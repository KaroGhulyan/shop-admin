import BaseTable from './base-table';
import { DB, shopDataBase } from './database'


export default class AttributesToAttrValues extends BaseTable {
    constructor() {
        super('attributesToAttrValues');
    }
    createObj({ id, attribute_id, attributeValues_id }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            attribute_id,
            attributeValues_id,
            remove() {
                // console.log(this.id)
                that._remove(this.id);
            }
        }
    }
    create({ attribute_id, attributeValues_id }) {
        let attrToval = this.createObj({ attribute_id, attributeValues_id })
        if (attributeValues_id.length) {
            this._add(attrToval);
        }
    }
}