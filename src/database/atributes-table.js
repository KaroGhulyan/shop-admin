import BaseTable from './base-table';
import { DB, shopDataBase } from './database'


export default class AttributesTable extends BaseTable {
    constructor() {
        super('attributes');
    }
    _createCategoryObject({ id, name }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            name,
            attributesValues() {
                let dataId = shopDataBase.attributesToAttrValues.getAll();
                let arr = [];
                dataId.forEach(element => {
                    if (element.attribute_id === this.id) {
                        element.attributeValues_id.forEach(id => {
                            arr.push(that._db.attributesValues._data.find(item => item.id === id))
                        })
                    }
                });
                return arr
            },
            getNames() {
                const names = this.attributesValues();
                let arr = names.map(element => {
                    if (element !== undefined) {
                        return element.name
                    }
                    // else {
                    //     console.log(element)

                    // }
                })
                return arr;
            },
            addAtrToAttrValues({ attributeValues_id, attribute_id }) {
                that._db.attributesToAttrValues.create({ attributeValues_id, attribute_id });
            },
            remove() {
                let arr = shopDataBase.attributesToAttrValues.getAll();
                for (let i = 0; i < arr.length; i++) {
                    if (arr[i].attribute_id === this.id) {
                        arr[i].remove(this.id);
                        i--;
                    }
                }
                that._remove(this.id);
            }
        }
    }
    create(AttributesData) {
        let attr = this._createCategoryObject(AttributesData)
        this._add(attr);
    }
}