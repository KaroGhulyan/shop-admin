import BaseTable from './base-table';
// import shopDataBase from '../index';
// import DB from './database';
// const shopDataBase = new DB("Shop Data Base");
// console.log
import {
    DB,
    shopDataBase
} from './database'



export default class CategoriesTable extends BaseTable {
    constructor() {
        super('categories')
    }
    _createCategoryObject({
        id,
        name
    }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            name,
            products() {
                return that._db.products.getByCategoryId(this.id)
            },

            attributesGroups() {
                let dataId = shopDataBase.attributesGroupsToCategory.getAll();
                let arr = [];
                dataId.forEach(element => {
                    if (element.category_id === this.id) {
                        element.attributeGroups_id.forEach(id => {
                            arr.push(that._db.attributeGroups._data.find(item => item.id === id))
                        })
                    }
                })
                return arr;
            },
            

            getNames() {
                const names = this.attributesGroups();
                let arr = names.map(element => {
                    if (element !== undefined) {
                        return element.name
                    } else {
                        let index = names.findIndex(element => element === undefined);
                        names.splice(index, 1, { name: '-' });
                    }
                    // else {

                    // }
                })
                return arr;

            },
            addAtrGroupToCategories({
                attributeGroups_id,
                category_id
            }) {
                // console.log(that._db)
                that._db.attributesGroupsToCategory.create({
                    attributeGroups_id,
                    category_id
                });
            },

            remove() {
                this.products().forEach(element => {
                    element.category_id = null;
                });
                let arr = shopDataBase.attributesGroupsToCategory.getAll();
                console.log(arr);
                for (let i = 0; i < arr.length; i++) {
                    if (arr[i].category_id === this.id) {
                        arr[i].remove(this.id);
                        i--;
                    }
                }
                that._remove(this.id);
            }
        }
    }
    create(categoryData) {
        let category = this._createCategoryObject(categoryData)
        this._add(category);
    }
}