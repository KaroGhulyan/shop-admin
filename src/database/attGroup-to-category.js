import BaseTable from './base-table';
import {
    DB,
    shopDataBase
} from './database'


export default class AttributeGroupsToCategories extends BaseTable {
    constructor() {
        super('attributesGroupsToCategory')
    };
    createObj({
        id,
        attributeGroups_id,
        category_id
    }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            attributeGroups_id,
            category_id,
            remove() {
                that._remove(this.id);
            }

        }
    }

    create({
        attributeGroups_id,
        category_id
    }) {
        let attrgrouptoCategory = this.createObj({
            attributeGroups_id,
            category_id
        });
        if (!shopDataBase.categories.getById(category_id)) {
            throw `attributeGroup id is doesn't exists`
        };
        if (attributeGroups_id.length) {
            this._add(attrgrouptoCategory);
            // attributeGroups_id.forEach((element,index,array) => {
            // this._add({
            //     id: this._idGenerate(),
            //     category_id,
            //     attributeGroups_id,
            // });
            // })
        }
    }
}