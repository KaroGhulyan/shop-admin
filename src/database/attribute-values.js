import BaseTable from './base-table';
import { DB, shopDataBase } from './database'

export default class AttributesValuesTable extends BaseTable {
    constructor() {
        super('attributesValues');
    }
    _createValuesObject({ id, name, }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            name,


            // attributes() {
            //     let dataId = shopDataBase.attributesToAttrValues.getAll();
            //     // console.log(dataId)
            //     let arr = [];
            //     return arr
            // },
            // getNames() {
            //     const names = this.attributes();
            //     let arr = names.map(element => element.name)
            //     return arr;
            // },
            // addAtrToAttrValues({ attribute_id, attributeGroups_id }) {
            //   that._db.attributesToAttrValues.create({ attribute_id, attributeGroups_id });
            // },
            remove() {

                that._remove(this.id);
            }
        }
    }
    create(AttributesValuesData) {
        let attrVal = this._createValuesObject(AttributesValuesData)
        this._add(attrVal);
    }
}