import BaseTable from './base-table';
import { shopDataBase } from './database'



export default class ProductInfo extends BaseTable {
    constructor() {
        super('productInfo');
    }
    _createObject({ id, product_id, attribute_id, attribute_value }) {
        const that = this;
        return {
            id: id || this._idGenerate(),
            product_id,
            attribute_id,
            attribute_value,
            remove() {
                that._remove(this.id);
            }
        };
    }
    getAttributesValues(id) {
        return this.getAll().filter(item => id === item.product_id);
    }
    create(productInfo) {
        const productData = this._createObject(productInfo);
        this._add(productData);
    }
}

