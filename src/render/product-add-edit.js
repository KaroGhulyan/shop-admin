import { shopDataBase } from '../database/database';
import state from "../render/state";
class ProductAddEdit {
    constructor() {
        // this.selectedCategoryInputs.bind(this)
    }
    renderOptions(selectDB) {
        let optionsInfo = selectDB.getAll();
        let template = '';
        optionsInfo.forEach(el => {
            template += `<option class="options" value ='${el.id}'>${el.name}</option>`;
        })
        return template;
    };
    renderForms(db) {
        // console.log(db)
        let template = '';
        let data = db._data[0];
        let obj = JSON.parse(JSON.stringify(data));
        let keys = Object.keys(obj).splice(1);
        keys.forEach(element => {
            template += `
          <div class="form-group col-md-4">
            <label for="${element}" class="cursor-pointer">${element}</label>
            <input type="text" required  class="form-control text-dark" id="${element}" placeholder="${element}" />
          </div>
            `
        })
        return template;
    }
    renderAttrGroups(data) {
        return data.reduce((acc, item) => acc += `<option  value ='${item.name}'>${item.name}</option>`, '')
    };

    //Render Selected Category Inputs
    selectedCategoryInputs(event) {
        event.preventDefault();
        const root = document.querySelector('#attributesInputs');
        let categoryId = event.target.selectedOptions[0].value
        let template = '';
        let attributes = [];
        let attrGroups = shopDataBase.categories.getById(categoryId).attributesGroups();
        attrGroups.forEach((attrGr) => {
            attributes.push(...attrGr.attributes())
        });
        attributes.forEach(attr => {
            template += `
        <label>${attr.name}</label>
        <select class="form-control js-example-tags selectAttributes" multiple="multiple" id="${attr.id}">
            ${productAddEdit.renderAttrGroups(attr.attributesValues())}
        </select>
          `
        })

        root.innerHTML = template;
        //MulstiSelect JS
        $(".js-example-tags").select2({
            tags: true
        });
    }
    addInfo(event) {
        const form = event.target.form
        let formItems = [];
        let productData = {
            name: '',
            price: '',
            category_id: ''
        };
        //Get Form Items
        for (const iterator of form) {
            formItems.push(iterator);
        };
        formItems.pop()
        //RemoveButtons
        for (const iterator of formItems) {
            if (!iterator.validity.valid) {
                return;
            }
        }
        event.preventDefault();

        productData.name = formItems[0].value
        productData.price = +formItems[1].value
        productData.category_id = +formItems[2].selectedOptions[0].value
        const product_id = shopDataBase.products.create(productData);
        const selAttr = document.querySelectorAll('.selectAttributes');
        selAttr.forEach(element => {
            for (const opt of element.selectedOptions) {
                shopDataBase.productInfo.create({
                    "product_id": product_id,
                    "attribute_id": +element.id,
                    'attribute_value': opt.value
                })
            }


        })
    }
    editInfo(e) {
        e.preventDefault();
        const selectedCategory = () => {
            let category;
            document.querySelectorAll('#selectCategory > option').forEach(element => {
                if (element.selected) category = element.value;
            });
            return category;
        };
        const name = document.querySelector('#name').value;
        const price = document.querySelector('#price').value;
        const category_id = selectedCategory();
        // console.log(shopDataBase.products.getById(state.id))
        shopDataBase.products.getById(state.id).name = name;
        shopDataBase.products.getById(state.id).price = price;
        shopDataBase.products.getById(state.id).category_id = category_id;
    }

    addProduct() {
        const selectElement = document.querySelector('#selectCategory');
        selectElement.onchange = this.selectedCategoryInputs;
        if (state.url === '/product/add') {
            const productAdd = document.querySelector('#add');
            productAdd.onclick = this.addInfo;
        }

        if (state.url === `/product/edit/${state.id}`) {
            const productAdd = document.querySelector('#add');
            productAdd.onclick = this.editInfo;
        }


        // this.renderAttrGroups();
    }

    render(currentDb, selectDB, id) {
        // this.selectedCategoryInputs()

        state.id = +id;
        state.currentDb = currentDb;
        let template = '';
        template = `
            <div class="mt-5">
                <form class="pt-5 pr-5 pl-5 pb-0 row d-flex flex-column">
                    ${this.renderForms(currentDb, id)}
                    <div class="form-group mt-4 col-md-4" id='form-group'>
                    <label>category</label>
                        <select id="selectCategory" required class="form-control">
                            <option data-id='choose'> select category </option>
                            ${this.renderOptions(selectDB)}
                        </select>
                        <div id='attributesInputs'>
                    
                        </div>
                    </div>
                    <button class='btn btn-primary add' id='add' type='submit'>
                        <a class="text-white">Save</a>
                    </button>
                </form>
                <button class='btn btn-primary ml-5 back'id='back'>
                        <a href='#/product' class="text-white">Back</a>
                </button>
            </div>
            `
        return template;
    }
}

const productAddEdit = new ProductAddEdit();
export { productAddEdit };
