// import { DB, shopDataBase } from '../database/database';
import { shopDataBase } from '../database/database';

class TableRender {
    constructor() {

    };
    renderTableHeader(keys) {
        let template = '';
        for (let i = 0; i < keys.length; i++) {
            template += `<th scope="col">${keys[i]}</th>`;
        }
        return template;
    }

    renderOptions(arr) {
        let names = '';
        arr.forEach(name => {
            names += `
            <option>${name}</option>
            `
        });
        return names;
    }
    renderTableTd(element) {
        // console.log(element);
        let template = '';
        for (const key in element) {
            if (typeof element[key] !== 'function') {
                template += `<td>${element[key]}</td >`
            } else {
                if (element[key].name === 'getNames') {
                    template += `<td>
                            <select class="form-control bg-info text-white">
                                ${this.renderOptions(element[key]())}
                            </select>
                       </td >`
                }
            }
        }

        return template;
    };
    renderTableBody(data, name) {
        let template = ''
        data.forEach(element => {
            Object.defineProperty(element, 'category_id', {
                enumerable: false,
            })
            template += `
            <tr data-id='${element.id}'>
                ${this.renderTableTd(element)}
                <td>
                    <button class="btn trashbtn p-0 ml-3" data-id='${element.id}'>
                        <i class="fas fa-trash-alt fa-2x text-danger"></i>
                    </button>
                </td>
                <td>
                <button class="btn p-0 m-0">
                    <a href="#/${name}/edit/${element.id}"  >
                        <i class="far fa-2x fa-edit text-white"></i>
                    </a>
                </button>
                </td>
            </tr>
            `
        })
        return template;
    }
}

const tableRender = new TableRender();
export default tableRender;