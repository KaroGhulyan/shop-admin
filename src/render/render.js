import tableRender from './table-render';
import state from "../render/state";

class Render {
  constructor() {

  }
  removeItem() {
    const trushBtn = document.querySelectorAll('.trashbtn');
    const removeBtn = document.getElementById('removeBtn');
    for (let index = 0; index < trushBtn.length; index++) {
      trushBtn[index].addEventListener('click', function (e) {
        e.preventDefault();
        $(document).ready(function () {
          $(".modal").modal('show');
        });

        state.currentDb.forEach(element => {
          if (+trushBtn[index].dataset.id === element.id) {
            removeBtn.onclick = function (e) {
              e.preventDefault();
              element.remove();
              window.location.hash = `/${state.name}/remove`;
              $(document).ready(function () {
                $(".modal").modal('hide');
              });
              window.location.hash = `/${state.name}`;
            }
          }
        });
      });
    }
  }
  pagination() {
    $(function () {
      const rowsPerPage = 5;
      const rows = $('#my-table tbody tr');
      const rowsCount = rows.length;
      const pageCount = Math.ceil(rowsCount / rowsPerPage); // avoid decimals
      const numbers = $('#numbers');
      numbers[0].innerHTML = ''
      // Generate the pagination.
      for (var i = 0; i < pageCount; i++) {
        numbers.append('<li><a href="#">' + (i + 1) + '</a></li>');
      }

      // Mark the first page link as active.
      $('#numbers li:first-child a').addClass('active');

      // Display the first set of rows.
      displayRows(1);

      // On pagination click.
      $('#numbers li a').click(function (e) {
        var $this = $(this);

        e.preventDefault();

        // Remove the active class from the links.
        $('#numbers li a').removeClass('active');

        // Add the active class to the current link.
        $this.addClass('active');

        // Show the rows corresponding to the clicked page ID.
        displayRows($this.text());
      });

      // Function that displays rows for a specific page.
      function displayRows(index) {
        var start = (index - 1) * rowsPerPage;
        var end = start + rowsPerPage;

        // Hide all rows.
        rows.hide();

        // Show the proper rows for this page.
        rows.slice(start, end).show();
      }
    })
  }
  search_table() {
    // Declare variables 
    var input, filter, table, tr, td, i, j;
    input = document.getElementById("search-input");
    filter = input.value.toUpperCase();
    table = document.getElementById("table-body");
    tr = table.getElementsByTagName("tr");

    // Loop through all table rows, and hide those who don't match the search query
    for (let i = 0; i < tr.length; i++) {
      td = tr[i].getElementsByTagName("td");
      for (let j = 0; j < td.length; j++) {
        let tdata = td[j];
        if (tdata) {
          if (tdata.innerHTML.toUpperCase().indexOf(filter) > -1) {
            tr[i].style.display = "";
            break;
          } else {
            tr[i].style.display = "none";
          }
        }
      }
    }
  }
  render(name, db, keys) {
    this.pagination()
    const data = db.getAll();
    state.currentDb = data;
    state.name = name;

    // Search
    $(document).ready(function () {
      $("#search-input").on("keyup", function () {
        const value = $(this).val().toLowerCase();
        $("#table-body tr").filter(function () {
          $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
      });
    });
    let template = ''
    template = `<div class="bg-dark" id='forminputs'>
      <div class="container">
      <div class='d-flex w-100 align-items-center justify-content-between'>
        <div>
          <h2 class="text-light mt-5 d-inline-block mr-5">Add ${name}</h2>
          <button type="button" class="btn btn-primary mb-3"  data-toggle="modal" data-target="#centralModalSm">
          <a href="#/${name}/add">
            <i class="fas fa-plus text-white px-3 font-size-25"></i>
          </a>
          </button>
        </div>
        <div class='mr-5 mt-5'>
          <form>
            <input class="form-control bg-light mr-sm-2 text-dark" type="search" placeholder="Search" id='search-input' aria-label="Search">
          </form>
        </div>
      </div>
       
        <table class="table table-hover table-dark" id="my-table">
        <thead>
        <tr>
          <th scope="col">#</th>
          ${tableRender.renderTableHeader(keys)}
          <th scope="col">remove</th>
          <th scope="col">edit</th>
        </tr>
      </thead>
      <tbody id='table-body'>
      ${tableRender.renderTableBody(data, name)}   
      </tbody>
        </table>
      </div>
    </div>
    `
    return template;
  }
}
const render = new Render();
export { render, Render };