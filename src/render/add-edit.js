import {
  shopDataBase
} from '../database/database';
import state from '../render/state';
class AddEdit {
  constructor() {

  }
  // render Opions
  renderOptions(data) {
    console.log(data)
    let optionsInfo = data.getAll();
    let template = '';
    optionsInfo.forEach(el => {
      template += `<option class="options" data-id ='${el.id}'>${el.name}</option>`;
    })
    return template;
  };
  renderForms(db) {

    let template = '';
    let data = db._data[0];
    // console.log(data)
    let obj = JSON.parse(JSON.stringify(data));
    let keys = Object.keys(obj).splice(1);
    keys.forEach(element => {
      template += `
      <div class="form-group col-md-6">
        <label for="${element}" class="cursor-pointer">${element}</label>
        <input type="text" class="form-control" id="${element}" placeholder="${element}" />
      </div>
        `
    })
    return template;
  }

  //ADD ITEMS
  addItemInfo(e) {
    e.preventDefault();
    let currentItemId;
    const selectedOptionValue = () => {
      let arr = [];
      document.querySelectorAll('#selectOptions > option').forEach(element => {
        if (element.selected) {
          arr.push(+element.dataset.id)
        }
      });
      return arr;
    };
    const id = selectedOptionValue();
    // console.log(id);
    const name = document.querySelector('#name').value;
    state.currentDb.create({ name });
    let arr = [...state.currentDb._data];
    currentItemId = arr.pop().id;
    if (state.url === `/categories/add`) {
      state.currentDb.getById(currentItemId).addAtrGroupToCategories({ attributeGroups_id: id, category_id: currentItemId })
    }
    if (state.url === `/attributes-groups/add`) {
      state.currentDb.getById(currentItemId).addAtrToAttrGroup({ attribute_id: id, attributeGroups_id: currentItemId })
    }
    if (state.url === `/attributes/add`) {
      state.currentDb.getById(currentItemId).addAtrToAttrValues({ attributeValues_id: id, attribute_id: currentItemId });
    }
  }

  //EDIT ITEMS
  editItemInfo(e) {
    e.preventDefault();
    // console.log(state.id)
    let currentItemId;
    const selectedOptionValue = () => {
      let arr = [];
      document.querySelectorAll('#selectOptions > option').forEach(element => {
        if (element.selected) {
          arr.push(+element.dataset.id)
        }
      });
      return arr;
    };
    const id = selectedOptionValue();
    const name = document.querySelector('#name').value;
    state.currentDb.getById(state.id).name = name;

    if (state.url === `/categories/edit/${state.id}`) {
      shopDataBase.attributesGroupsToCategory.getById(state.id).attributeGroups_id = id;
    }
    if (state.url === `/attributes-groups/edit/${state.id}`) {
      shopDataBase.attributesGroupsToAttribute.getById(state.id).attribute_id = id
    }
    if (state.url === `/attributes/edit/${state.id}`) {
      shopDataBase.attributesToAttrValues.getById(state.id).attributeValues_id = id
      // state.currentDb.getById(currentItemId).addAtrToAttrValues({ attributeValues_id: id, attribute_id: currentItemId });
    }
  }


  //ADD/EDIT Event Listeners
  addItem() {
    $(".js-example-tags1").select2({
      tags: true
    });
    if (state.url === `/${state.name}/add`) {
      const add = document.querySelector('#add');
      add.addEventListener('click', this.addItemInfo);
    }
    if (state.url === `/${state.name}/edit/${state.id}`) {
      const add = document.querySelector('#add');
      add.addEventListener('click', this.editItemInfo)
    }
  };
  render(name, currentDb, selectDB, id) {
    console.log(selectDB)
    state.currentDb = currentDb;
    state.id = id;
    state.name = name;
    let template = '';
    template = `
             <div class="mt-5">
              <form class="p-5 row d-flex flex-column">
             <h3 class='ml-3 text-muted'>Add/Edit ${name}</h3>
                  ${this.renderForms(currentDb)}
                  <div class="form-group mt-4 col-md-4">
                  <select class="form-control js-example-tags1" multiple="multiple" id="selectOptions">
                    ${this.renderOptions(selectDB)}
                    </select>
                  </div>
              </form>
                    
                      <button class='btn btn-primary ml-5'>
                          <a href='#/${name}' class="text-white">Back</a>
                      </button>
                      <button class='btn btn-primary' id='add'>
                          <a class="text-white">Save</a>
                      </button>
             </div>
        `
    return template;
  }
}

const addEdit = new AddEdit();

export {
  AddEdit,
  addEdit
};