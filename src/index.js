import ProductTable from './database/products-table';
import BaseTable from './database/base-table';
import CategoriesTable from './database/categories-table';
import AttributeGroupsTable from './database/atributes-groups';
import AttributesTable from './database/atributes-table';
import AttributesValuesTable from './database/attribute-values';
import AttributeGroupsToCategories from './database/attGroup-to-category';
import AttributeGroupsToAttribute from './database/attGroupt-to-attr';
import AttributesToAttrValues from './database/attr-to-attrVal';
import UsersTable from './database/users-table';
import { DB, shopDataBase } from './database/database';
import router from './route/router';
import tableRender from './render/table-render';
import ProductInfo from './database/product-Info';


window.shopDataBase = shopDataBase;
console.log(shopDataBase)


// function _idGenerate() {
//     let i = 1;
//     return function () {
//         return i++;
//     };
// }
// // const shopDataBase = new DB("Shop Data Base");
// // console.log(shopDataBase);
shopDataBase.addTables([
    new ProductTable(),
    new CategoriesTable(),
    new AttributesTable(),
    new AttributesValuesTable(),
    new UsersTable(),
    new AttributeGroupsTable(),
    new AttributeGroupsToAttribute(),
    new AttributeGroupsToCategories(),
    new AttributesToAttrValues(),
    new ProductInfo(),
]);


// console.log(products.render())

// categories
shopDataBase.categories.create({ name: 'Shoes', });
shopDataBase.categories.create({ name: 'Clothing', });
shopDataBase.categories.create({ name: 'Accessories', });

// shopDataBase.categories.getById(1).remove();
// attributeGroups
shopDataBase.attributeGroups.create({ name: 'Running', });
shopDataBase.attributeGroups.create({ name: 'Tops & T-Shirts', });
shopDataBase.attributeGroups.create({ name: 'Backpacks', });
// attributes
shopDataBase.attributes.create({ name: 'Size' });
shopDataBase.attributes.create({ name: 'Color' });
shopDataBase.attributes.create({ name: 'Weight' });
shopDataBase.attributes.create({ name: 'Sex' });
//values
shopDataBase.attributesValues.create({ name: 'male' });
shopDataBase.attributesValues.create({ name: 'female' });
shopDataBase.attributesValues.create({ name: 'S' });
shopDataBase.attributesValues.create({ name: 'M' });
shopDataBase.attributesValues.create({ name: 'L' });
shopDataBase.attributesValues.create({ name: 'Red' });
shopDataBase.attributesValues.create({ name: 'Blue' });
shopDataBase.attributesValues.create({ name: 'Orange' });
shopDataBase.attributesValues.create({ name: 'Synthetic' });
shopDataBase.attributesValues.create({ name: 'Rubber' });
shopDataBase.attributesValues.create({ name: 'kg' });
shopDataBase.attributesValues.create({ name: '12' });
shopDataBase.attributesValues.create({ name: '8' });
shopDataBase.attributesValues.create({ name: '6' });


shopDataBase.categories.getById(1).addAtrGroupToCategories({ attributeGroups_id: [1], category_id: 1 })
shopDataBase.categories.getById(2).addAtrGroupToCategories({ attributeGroups_id: [2], category_id: 2 })
shopDataBase.categories.getById(3).addAtrGroupToCategories({ attributeGroups_id: [3], category_id: 3 })

shopDataBase.attributeGroups.getById(1).addAtrToAttrGroup({ attribute_id: [1, 2], attributeGroups_id: 1 })
shopDataBase.attributeGroups.getById(2).addAtrToAttrGroup({ attribute_id: [2, 3], attributeGroups_id: 2 })
shopDataBase.attributeGroups.getById(3).addAtrToAttrGroup({ attribute_id: [3, 4], attributeGroups_id: 3 })


shopDataBase.attributes.getById(1).addAtrToAttrValues({ attributeValues_id: [12, 13, 14], attribute_id: 1 });
shopDataBase.attributes.getById(2).addAtrToAttrValues({ attributeValues_id: [6, 7, 8], attribute_id: 2 });
shopDataBase.attributes.getById(3).addAtrToAttrValues({ attributeValues_id: [11], attribute_id: 3 });
shopDataBase.attributes.getById(4).addAtrToAttrValues({ attributeValues_id: [1, 2], attribute_id: 4 });

shopDataBase.productInfo.create({ product_id: 1, attribute_id: 1, attribute_value: 'S' });
shopDataBase.productInfo.create({ product_id: 1, attribute_id: 1, attribute_value: 'M' });
shopDataBase.productInfo.create({ product_id: 1, attribute_id: 1, attribute_value: 'L' });
shopDataBase.productInfo.create({ product_id: 1, attribute_id: 2, attribute_value: 'Red' });
shopDataBase.productInfo.create({ product_id: 1, attribute_id: 2, attribute_value: 'Blue' });
shopDataBase.productInfo.create({ product_id: 1, attribute_id: 2, attribute_value: 'Orange' });
shopDataBase.productInfo.create({ product_id: 1, attribute_id: 3, attribute_value: 'kg' });
shopDataBase.productInfo.create({ product_id: 1, attribute_id: 4, attribute_value: 'male' });
shopDataBase.productInfo.create({ product_id: 1, attribute_id: 4, attribute_value: 'female' });


shopDataBase.productInfo.create({ product_id: 2, attribute_id: 1, attribute_value: 'S' });
shopDataBase.productInfo.create({ product_id: 2, attribute_id: 1, attribute_value: 'M' });
shopDataBase.productInfo.create({ product_id: 2, attribute_id: 1, attribute_value: 'L' });
shopDataBase.productInfo.create({ product_id: 2, attribute_id: 2, attribute_value: 'Red' });
shopDataBase.productInfo.create({ product_id: 2, attribute_id: 2, attribute_value: 'Blue' });
shopDataBase.productInfo.create({ product_id: 2, attribute_id: 2, attribute_value: 'Orange' });
shopDataBase.productInfo.create({ product_id: 2, attribute_id: 3, attribute_value: 'kg' });
shopDataBase.productInfo.create({ product_id: 2, attribute_id: 4, attribute_value: 'male' });
shopDataBase.productInfo.create({ product_id: 2, attribute_id: 4, attribute_value: 'female' });


// shopDataBase.attributes.getById(1).attributesValues();
// shopDataBase.attributes.getById(2).attributesValues();


// console.log(shopDataBase.attributes.getById(2).attributesValues())



// console.log()



shopDataBase.users.create({
    name: 'Karo',
    surname: 'Ghulyan',
    sex: 'male',
    age: '26',
    email: 'kghulyan93@gmail.com',
    adress: 'New Nork'
});
shopDataBase.users.create({
    name: 'Anna',
    surname: 'Ghulyan',
    sex: 'female',
    age: '26',
    email: 'kghulyan93@gmail.com',
    adress: 'New Nork'
});
shopDataBase.users.create({
    name: 'Erik',
    surname: 'Galstyan',
    sex: 'male',
    age: '26',
    email: 'kghulyan93@gmail.com',
    adress: 'New Nork'
});
shopDataBase.users.create({
    name: 'Erik',
    surname: 'Galstyan',
    sex: 'male',
    age: '26',
    email: 'kghulyan93@gmail.com',
    adress: 'New Nork'
});

shopDataBase.products.create({ name: 'Nike Shoe Predator', price: 50, category_id: 1 });
shopDataBase.products.create({ name: 'Nike Air Prediction', price: 50, category_id: 1 });
shopDataBase.products.create({ name: 'Nike Yoga DR Fit', price: 42, category_id: 2 });
shopDataBase.products.create({ name: 'Nike Rise 365', price: 42, category_id: 2 });
shopDataBase.products.create({ name: 'Nike SB RPM', price: 22, category_id: 3 });
shopDataBase.products.create({ name: 'Nike Heritage', price: 22, category_id: 3 });


// console.log(shopDataBase.categories.getById(1).remove());


(function ($) {

    "use strict";

    var fullHeight = function () {

        $('.js-fullheight').css('height', $(window).height());
        $(window).resize(function () {
            $('.js-fullheight').css('height', $(window).height());
        });

    };
    fullHeight();

    $('#sidebarCollapse').on('click', function () {
        $('#sidebar').toggleClass('active');
    });

})(jQuery);


router();


