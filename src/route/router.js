import UniversalRouter from 'universal-router';

import { shopDataBase } from '../database/database';
import { addEdit } from '../render/add-edit';
import { productAddEdit } from '../render/product-add-edit';
import { render } from '../render/render';
import state from '../render/state'
export default function router() {

    //DB
    let products = shopDataBase.products;
    let categories = shopDataBase.categories;
    let attributesGroups = shopDataBase.attributeGroups;
    let attributes = shopDataBase.attributes;
    let attributesValues = shopDataBase.attributesValues;

    // let attToCat = 

    //Table Header
    let productsKey = ['name', 'price', 'categories'];
    let attributesGroupsKey = ['name', 'attributes'];
    let categoriesKey = ['name', 'attributes-groups'];
    let attributesKey = ['name', 'attributes-values'];
    let attributesValuesKeys = ['name'];


    const appDiv = "content";


    const routes = [
        {
            path: '',
            action: () => { return { html: render.render(products, productsKey) } },
        },
        //products
        {
            path: '/product',
            action: null,
            children: [
                {
                    path: '',
                    action: () => {
                        return {
                            html: render.render('product', products, productsKey),
                            callback: render.removeItem.bind(render),
                            callback1: render.search_table.bind(render)
                        }
                    }
                },
                {
                    path: '/add',
                    action: () => {
                        return {
                            html: productAddEdit.render(products, categories),
                            callback: productAddEdit.addProduct.bind(productAddEdit),
                            // callback1: productAddEdit.addInput.bind(productAddEdit)
                        }
                    }
                },
                {
                    path: '/edit/:id',
                    action: (context) => {
                        return {
                            html: productAddEdit.render(products, categories, context.params.id),
                            callback: productAddEdit.addProduct.bind(productAddEdit),
                        }
                    },
                },
            ],
        },

        //categories
        {
            path: '/categories',
            action: null,
            children: [
                {
                    path: '',
                    action: () => {
                        return {
                            html: render.render('categories', categories, categoriesKey),
                            callback: render.removeItem.bind(render),
                            callback1: render.search_table.bind(render)

                        }
                    }
                },
                {
                    path: '/add',
                    action: () => {
                        return {
                            html: addEdit.render('categories', categories, attributesGroups),
                            callback: addEdit.addItem.bind(addEdit),
                        }
                    }
                },
                {
                    path: '/edit/:id',
                    action: (context) => {
                        return {
                            html: addEdit.render('categories', categories, attributesGroups, context.params.id),
                            callback: addEdit.addItem.bind(addEdit),
                        }
                    }
                },
            ],
        },
        // Attributes-Groups
        {
            path: '/attributes-groups',
            action: null,
            children: [
                {
                    path: '',
                    action: () => {
                        return {
                            html: render.render('attributes-groups', attributesGroups, attributesGroupsKey),
                            callback: render.removeItem.bind(render),
                            callback1: render.search_table.bind(render)
                        }
                    }
                },
                {
                    path: '/add',
                    action: () => {
                        return {
                            html: addEdit.render('attributes-groups', attributesGroups, attributes),
                            callback: addEdit.addItem.bind(addEdit),
                        }
                    }
                },
                {
                    path: '/edit/:id',
                    action: (context) => {
                        return {
                            html: addEdit.render('attributes-groups', attributesGroups, attributes, context.params.id),
                            callback: addEdit.addItem.bind(addEdit),
                        }
                    }
                },
            ],
        },
        // // Attributes
        {
            path: '/attributes',
            action: null,
            children: [
                {
                    path: '',
                    action: () => {
                        return {
                            html: render.render('attributes', attributes, attributesKey),
                            callback: render.removeItem.bind(render),
                            callback1: render.search_table.bind(render)
                        }
                    }
                },
                {
                    path: '/add',
                    action: () => {
                        return {
                            html: addEdit.render('attributes', attributes, attributesValues),
                            callback: addEdit.addItem.bind(addEdit),
                        }
                    }
                },
                {
                    path: '/edit/:id',
                    action: (context) => {
                        return {
                            html: addEdit.render('attributes', attributes, attributesValues, context.params.id),
                            callback: addEdit.addItem.bind(addEdit),
                        }
                    }
                },
            ],
        },
        // // Attributes-values
        {
            path: '/attributes-values',
            action: null,
            children: [
                {
                    path: '',
                    action: () => {
                        return {
                            html: render.render('attributes-values', attributesValues, attributesValuesKeys),
                            callback: render.removeItem.bind(render),
                            callback1: render.search_table.bind(render)
                        }
                    }
                },
                {
                    path: '/add',
                    action: () => {
                        return {
                            html: addEdit.render('attributes-values', attributesValues, attributes),
                            callback: addEdit.addItem.bind(addEdit),
                        }
                    }
                },
                {
                    path: '/edit/:id',
                    action: (context) => {
                        return {
                            html: addEdit.render('attributes-values', attributesValues, context.params.id),
                            callback: addEdit.addItem.bind(addEdit),
                        }
                    }
                },
            ],
        },

    ];

    const router = new UniversalRouter(routes);
    window.router = router;
    let routing = () => {
        const url = window.location.hash.slice(1);
        router.resolve(url).then(({ html, callback, callback1 }) => {
            state.url = url;
            const renderContent = document.getElementById(appDiv);
            // renderContent.innerHTML = '';
            renderContent.innerHTML = html;
            // if (typeof callback1 === 'function') {
            //     callback1();
            // }
            if (typeof callback === 'function') {
                callback();
            }
            if (typeof callback1 === 'function') {
                // callback1();
            }


        });
    };
    window.addEventListener('hashchange', routing);
    routing();
}
